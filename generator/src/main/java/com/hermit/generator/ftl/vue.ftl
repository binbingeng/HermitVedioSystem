<template>
  <!-- PAGE CONTENT BEGINS -->
  <div>
    <p>
      <button v-on:click="add()" class="btn btn-white btn-warning btn-round">
        <i class="ace-icon fa fa-edit bigger-120 blue"></i>
        新增
      </button>
      &nbsp;
      <button v-on:click="list(1)" class="btn btn-white btn-warning btn-round">
        <i class="ace-icon fa fa-refresh bigger-120 blue"></i>
        刷新
      </button>
    </p>
    <paginations ref="paginations" v-bind:list="list"></paginations>
    <table id="simple-table" class="table  table-bordered table-hover">
      <thead>
      <tr><#list fieldList as field><#if field.nameHump!="createdAt" &&field.nameHump!="updatedAt">
        <th> ${field.nameCn} </th></#if></#list>
        <th>操作</th>
      </tr>
      </thead>
      <tbody>
      <tr v-for="${domain} in ${domain}s" :key="${domain}.num">
        <#list fieldList as field><#if field.nameHump!="createdAt" &&field.nameHump!="updatedAt">
          <#if field.enums>
          <td> {{${field.enumsConst} | optionKV(${domain}.${field.nameHump})}} </td>
          <#else>
          <td> {{${domain}.${field.nameHump}}} </td>
          </#if>
        </#if></#list>
        <td>
          <div class="hidden-sm hidden-xs btn-group">
            <button v-on:click="edit(${domain})" class="btn btn-xs btn-info">
              <i class="ace-icon fa fa-pencil bigger-120"></i>
            </button>

            <button v-on:click="del(${domain}.id)" class="btn btn-xs btn-danger">
              <i class="ace-icon fa fa-trash-o bigger-120"></i>
            </button>
          </div>

          <div class="hidden-md hidden-lg">
            <div class="inline pos-rel">
              <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
              </button>

              <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                <li>
                  <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
                    <span class="blue">
                      <i class="ace-icon fa fa-search-plus bigger-120"></i>
                    </span>
                  </a>
                </li>

                <li>
                  <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                    <span class="green">
                      <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                    </span>
                  </a>
                </li>

                <li>
                  <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                    <span class="red">
                      <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </td>
      </tr>
      </tbody>
    </table>
    <!--模态框-->
    <div class="modal fade" id="myFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">表单</h4>
          </div>
          <div class="modal-body">
            <!--表格-->
            <form class="form-horizontal">
              <#list fieldList as field>
                <#if field.nameHump!="id" && field.nameHump!="createdAt" && field.nameHump!="updatedAt">
              <div class="form-group">
                <label class="col-sm-2 control-label">${field.nameCn}</label>
                <div class="col-sm-10">
                  <#if field.enums>
                  <select v-model="${domain}.${field.nameHump}" class="form-control">
                    <option v-for="o in ${field.enumsConst}" v-bind:value="o.key" :key="o.num">{{o.value}}</option>
                  </select>
                  <#else>
                  <input v-model="${domain}.${field.nameHump}" class="form-control" placeholder="输入${field.nameCn}"/>
                  </#if>
                </div>
              </div>
                </#if>
              </#list>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            <button type="button" class="btn btn-primary" v-on:click="save()">保存</button>
          </div>
        </div><!-- /.modal-content-->
      </div><!-- /.modal-dialog-->
    </div><!-- /.modal-->
  </div>
</template>
<script>
import Paginations from "../../components/paginations";
export default {
  name: '${module}-${domain}',
  components: {Paginations},
  data: function (){
    return{
      //映射表单数据
      ${domain}: {},
      ${domain}s: [],
      <#list fieldList as field>
        <#if field.enums>
      ${field.enumsConst}:${field.enumsConst},
        </#if>
      </#list>
    }
  },
  mounted:function (){
    let _this = this;
    _this.list(1);
  },
  methods:{

    add() {
      let _this = this;
      _this.${domain} = {};
      $("#myFormModal").modal("show");
    },

    edit(${domain}) {
      let _this = this;
      _this.${domain} = $.extend({},${domain});
      $("#myFormModal").modal("show");
    },
    /**查询*/
    list(page){
      let _this = this;
      Loading.show();
      _this.$ajax.post(process.env.VUE_APP_SERVER + '/${module}/admin/${domain}/list',{
        page:page,
        size:_this.$refs.paginations.size,
      }).then((response)=>{
        Loading.hide();
        console.log("查询${tableNameCn}列表结果：",response);
        let resp = response.data;
        _this.${domain}s = resp.content.list;
        _this.$refs.paginations.render(page,resp.content.total);
      })
    },
    /**保存*/
    save(){
      let _this = this;
      /**保存校验*/
      if(1 != 1
      <#list fieldList as field>
      <#if field.nameHump!="id" && field.nameHump!="createdAt" && field.nameHump!="updatedAt" && field.nameHump!="sort">
        <#if !field.nullAble>
      || !Validator.require(_this.${domain}.${field.name},"${field.nameCn}")
        </#if>
        <#if (field.length > 0)>
      || !Validator.length(_this.${domain}.${field.name},"${field.nameCn}",1,${field.length?c})
        </#if>
      </#if>
      </#list>
      ){
        return;
      }
      _this.$ajax.post(process.env.VUE_APP_SERVER + '/${module}/admin/${domain}/save',_this.${domain})
      .then((response)=>{
        console.log("保存${tableNameCn}列表结果：",response);
        let resp = response.data;
        if(resp.success){
          $("#myFormModal").modal("hide");
          _this.list(1);
          toast.success("保存成功！");
        }else{
          toast.warning(resp.message);
        }


      })
    },
    /**删除*/
    del(id){
      let _this = this;
      //弹出框
      Confirm.show("删除${tableNameCn}后不可恢复，您是否检查删除!",function (){
        _this.$ajax.delete(process.env.VUE_APP_SERVER + '/${module}/admin/${domain}/delete/'+ id )
            .then((response)=>{
              console.log("删除${tableNameCn}列表结果：",response);
              let resp = response.data;
              if(resp.success){
                _this.list(1);
                toast.success("删除成功！");
              }
            });
      })
    },
  }
}
</script>