package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.${Domain};
import com.hermit.server.domain.${Domain}Example;
import com.hermit.server.dto.${Domain}Dto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.${Domain}Mapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
<#list typeSet as type>
    <#if type="Date">
import java.util.Date;
    </#if>
</#list>


@Service
public class ${Domain}Service {
    @Resource
    private ${Domain}Mapper ${domain}Mapper;

    /** 查询*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        ${Domain}Example ${domain}Example = new ${Domain}Example();
        /** 根据sort来排序*/
        <#list fieldList as field>
            <#if field.nameHump=='sort'>
        ${domain}Example.setOrderByClause("sort asc");
            </#if>
        </#list>

        List<${Domain}> ${domain}List = ${domain}Mapper.selectByExample(${domain}Example);
        //查询数据
        PageInfo<${Domain}> pageInfo = new PageInfo<>(${domain}List);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<${Domain}Dto> ${domain}DtoList = new ArrayList<${Domain}Dto>();
        for (int i = 0; i < ${domain}List.size(); i++) {
            ${Domain} ${domain} = ${domain}List.get(i);
            ${Domain}Dto ${domain}Dto = new ${Domain}Dto();
            BeanUtils.copyProperties(${domain},${domain}Dto);
            ${domain}DtoList.add(${domain}Dto);
        }
        //设置最终查询结果
        pageDto.setList(${domain}DtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    public void save(${Domain}Dto ${domain}Dto){
        ${Domain} ${domain} = CopyUtil.copy(${domain}Dto,${Domain}.class);
        if(StringUtils.isEmpty(${domain}Dto.getId())){
            this.insert(${domain});
        }else{
            this.update(${domain});
        }

    }
    /**新增 */
    private void insert(${Domain} ${domain}){
        Date now = new Date();
        <#list fieldList as field>
            <#if field.nameHump=='createdAt'>
        ${domain}.setCreatedAt(now);
            </#if>
            <#if field.nameHump=='updatedAt'>
        ${domain}.setUpdatedAt(now);
            </#if>
        </#list>
        ${domain}.setId(UUidUtil.getShortUuid());
        ${domain}Mapper.insert(${domain});
    }
    /**修改更新 */
    private void update(${Domain} ${domain}){
        <#list fieldList as field>
            <#if field.nameHump=='updatedAt'>
        ${domain}.setUpdatedAt(new Date());
            </#if>
        </#list>
        ${domain}Mapper.updateByPrimaryKey(${domain});
    }
    /**删除 */
    public void delete(String id){
        ${domain}Mapper.deleteByPrimaryKey(id);
    }
}