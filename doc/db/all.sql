这个页面非常非常非常重要
持久层代码根据这生成
dto，服务，控制根据generatorconfig.xml生成
vue代码生成
枚举类根据comment包含”枚举“来判断，更具【】中的String指向枚举类，再生成前端枚举代码

--------------------大章表设计-------------------------------------
drop table if exists `chapter`;
create table `chapter`(
    `id` char(8) not null  comment 'ID',
    `name` varchar(50) comment '名称',
    `course_id` char(8) comment '课程ID',
    primary key (`id`)
)engine=innodb default charset=utf8 comment='大章';

insert into `chapter` (`id`,`course_id`,`name`) values ('111','111','大章节1');
insert into `chapter` (`id`,`course_id`,`name`) values ('222','222','大章节2');

--------------------测试表设计--------------------------------------

drop table if exists `test`;
create table `test` (
`id`  char(8) not null default '' comment 'id',
`name`  varchar(50) comment '名称' ,
primary key (`id`)
)engine=innodb default charset=utf8 comment='测试';

insert into `test` (`id`,`name`) values (1,'测试1111');

--------------------小结表设计-------------------------------------
drop table if exists `section`;
create table `section`(
                          `id` char(8) not null default '' comment 'ID',
                          `title` varchar(50) not null comment '标题',
                          `course_id` char(8) comment '课程|course.id',
                          `chapter_id` char(8) comment '大章|chapter.id',
                          `video` varchar(200) comment '视频',
                          `time` int comment '时长|单位秒',
                          `charge` char(1) comment '收费|c 收费；f 免费',
                          `sort` int comment '顺序',
                          `created_at` datetime comment '创建时间',
                          `updated_at` datetime comment '修改时间',
                          primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='小节';

insert into `section` (`id`, `title`, `course_id`, `chapter_id`, `video`, `time`, `charge`, `sort`, `created_at`,
                       `updated_at`)
values ('00000001', '测试小姐01', '00000001', '00000000', ' ', 500, 'F', '1', now(), now());


--------------------课程表设计-------------------------------------
drop table if exists `course`;
create table `course`
(
    `id`         char(8)       not null default '' comment 'ID',
    `name`       varchar(50)   not null comment '名称',
    `summary`    varchar(2000) comment '概述',
    `time`       int                    default 0 comment '时长|单位秒',
    `price`      decimal(8, 2) not null default 0.00 comment '价格（元）',
    `image`      varchar(100) comment '封面',
    `level`      char(1)       not null comment '级别|枚举[CourseLevelEnum] : ONE("1","初级"),TWO("2","中级"),THREE("3","高级")',
    `charge`     char(1) comment '收费|枚举[CourseChargeEnum] : CHARGE("C","收费"),FREE("F","免费")',
    `status`     char(1) comment '状态|枚举[CourseStatusEnum] : PUBLISH("P","发布"),DRAFT("D","草稿")',
    `enroll`     integer                default 0 comment '报名数',
    `sort`       int comment '顺序',
    `created_at` datetime comment '创建时间',
    `updated_at` datetime comment '修改时间',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='课程';

--------课程表添加字段-------
alter table `course`
    add column (`teacher_id` char(8) comment '讲师|teacher.id') --------------------分类表设计-------------------------------------
drop table if exists `category`;
create table `category`
(
    `id`     char(8)     not null default '' comment 'id',
    `parent` char(8)     not null default '' comment '父id',
    `name`   varchar(50) not null comment '名称',
    `sort`   int comment '顺序',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='分类';

--------------------课程-分类连接表设计-------------------------------------
drop table if exists `course_category`;
create table `course_category`
(
    `id`          char(8) not null default '' comment 'id',
    `course_id`   char(8) comment '课程|course.id',
    `category_id` char(8) comment '分类|course.id',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='课程分类';

--------------------课程内容表设计-------------------------------------
drop table if exists `course_content`;
create table `course_content`
(
    `id`      char(8)    not null default '' comment '课程id',
    `content` mediumtext not null comment '课程内容',
    primary key (`id`)
) engine = innodb
  default charset = utf8;

--------------------讲师表设计-------------------------------------
drop table if exists `teacher`;
create table `teacher`
(
    `id`       char(8)     not null default '' comment 'id',
    `name`     varchar(50) not null comment '姓名',
    `nickname` varchar(50) comment '昵称',
    `image`    varchar(100) comment '头像',
    `position` varchar(50) comment '职位',
    `motto`    varchar(50) comment '座右铭',
    `intro`    varchar(500) comment '简介',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='讲师';

--------------------文件表设计-------------------------------------
drop table if exists `file`;
create table `file`
(
    `id` char(8) not null default '' comment 'id',
    `path` varchar(100) not null comment '相对路径',
    `name` varchar(100) comment '文件名',
    `suffix` varchar(10) comment '后缀',
    `size` int comment '大小|字节B',
    `use` char(1) comment '用途|枚举[FileUseEnum]:COURSE("C","讲师"),TEACHER("T","课程")',
    `created_at` datetime comment '创建时间',
    `updated_at` datetime comment '修改时间',
    primary key (`id`),
    unique key `path_unique` (`path`)
) engine = innodb
  default charset = utf8 comment ='文件';
  -----------文件表增加字段（断点上传 文件合并）--------
alter table `file` add column (`shard_index` int comment '已上传分片');
alter table `file` add column (`shard_size` int comment '分片大小|B');
alter table `file` add column (`shard_total` int comment '分片总数');
alter table `file` add column (`key` varchar(32) comment '文件标识');
alter table `file` add unique key key_unique (`key`);

--------------------课程类容文件表设计-------------------------------------
drop table if exists `course_content_file`;
create table `course_content_file`
(
    `id` char(8) not null default '' comment 'id',
    `course_id` char(8) not null comment '课程id',
    `name` varchar(100) comment '文件名',
    `url` varchar(100) comment '地址',
    `size` int comment '大小|字节B',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='课程内容文件';

--------------------用户表设计-------------------------------------
drop table if exists `user_info`;
create table `user_info`
(
    `id` char(8) not null default '' comment 'id',
    `username` varchar(50) not null comment '登陆名',
    `name` varchar(50) comment '昵称',
    `password` char(32) not null comment '密码',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='用户';

--------------------资源表设计-------------------------------------
drop table if exists `resource`;
create table `resource`
(
    `id` char(6) not null default '' comment 'id',
    `name` varchar(100) not null comment '名称|菜单或按钮',
    `page` varchar(50) comment '页面|路由',
    `request` varchar(200) comment '请求|接口',
    `parent` char(6) comment '父id',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='资源';

--------------------角色表设计-------------------------------------
drop table if exists `role`;
create table `role`
(
    `id` char(8) not null default '' comment 'id',
    `name` varchar(50) not null comment '角色',
    `desc` varchar(100) not null comment '描述',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='角色';

--------------------角色-资源 表设计-------------------------------------
drop table if exists `role_resource`;
create table `role_resource`
(
    `id` char(8) not null default '' comment 'id',
    `role_id` char(8) not null comment '角色|id',
    `resource` char(6) not null comment '资源|id',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='角色资源关联';

--------------------角色-用户 表设计-------------------------------------
drop table if exists `role_user`;
create table `role_user`
(
    `id` char(8) not null default '' comment 'id',
    `role_id` char(8) not null comment '角色|id',
    `user_id` char(8) not null comment '用户|id',
    primary key (`id`)
) engine = innodb
  default charset = utf8 comment ='角色用户关联';

