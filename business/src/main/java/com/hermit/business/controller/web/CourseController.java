package com.hermit.business.controller.web;

import com.hermit.server.dto.CourseDto;
import com.hermit.server.dto.CoursePageDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.enums.CourseStatusEnum;
import com.hermit.server.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController("webCourseController")
@RequestMapping("/web/course")
public class CourseController {
    private static final Logger LOG = LoggerFactory.getLogger(CourseController.class);
    public static final String BUSINESS_NAME = "课程";

    @Resource
    private CourseService courseService;

    @PostMapping("/list-new")
    public ResponseDto listNew(){
        PageDto pageDto = new PageDto();
        pageDto.setPage(1);
        pageDto.setSize(3);
        ResponseDto responseDto = new ResponseDto();
        List<CourseDto> courseDtoList = courseService.listNew(pageDto);
        responseDto.setContent(courseDtoList);
        return responseDto;
    }

    @PostMapping("/list-hot")
    public ResponseDto listHot(){
        PageDto pageDto = new PageDto();
        pageDto.setPage(1);
        pageDto.setSize(3);
        ResponseDto responseDto = new ResponseDto();
        List<CourseDto> courseDtoList = courseService.listHot(pageDto);
        responseDto.setContent(courseDtoList);
        return responseDto;
    }

    @PostMapping("/list")
    public ResponseDto list(@RequestBody CoursePageDto pageDto){
        ResponseDto responseDto = new ResponseDto();
        pageDto.setStatus(CourseStatusEnum.PUBLISH.getCode());
        courseService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    /**
     * web页面根据id查询courseDto所有信息 （包括课程信息，教师，小节，大章，描述）
     */
    @GetMapping("/find/{id}")
    public ResponseDto findCourse(@PathVariable String id){
        LOG.info("查询课程所有信息（web）");
        ResponseDto responseDto = new ResponseDto();
        CourseDto courseDto = courseService.findCourse(id);
        responseDto.setContent(courseDto);
        return responseDto;
    }
}
