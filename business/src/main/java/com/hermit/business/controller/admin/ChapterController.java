package com.hermit.business.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.Chapter;
import com.hermit.server.dto.ChapterDto;
import com.hermit.server.dto.ChapterPageDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.ChapterService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/chapter")
public class ChapterController {

    private static final Logger LOG = LoggerFactory.getLogger(ChapterController.class);
    public static final String BUSINESS_NAME = "大章";
    @Resource
    private ChapterService chapterService;

    @PostMapping("/list")
   public ResponseDto list(@RequestBody ChapterPageDto chapterPageDto){
        LOG.info("pageDto: {}",chapterPageDto);
        ResponseDto responseDto = new ResponseDto();
        ValidatorUtil.required(chapterPageDto.getCourseId(),"课程ID");
        chapterService.list(chapterPageDto);
        responseDto.setContent(chapterPageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody ChapterDto chapterDto){
        LOG.info("chapterDto: {}",chapterDto);

        /**
         * 防止前端获得不到返回数据，造成卡死，需要使用try包围  ctrl+alt+t
         * 此方法封装到ControllerExceptionHandler类中
         */
        ValidatorUtil.required(chapterDto.getName(),"名称");
        ValidatorUtil.required(chapterDto.getCourseId(),"课程ID");
        ValidatorUtil.length(chapterDto.getCourseId(),"课程ID",1,8);


        ResponseDto responseDto = new ResponseDto();
        chapterService.save(chapterDto);
        responseDto.setContent(chapterDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        chapterService.delete(id);
        return responseDto;
    }

}
