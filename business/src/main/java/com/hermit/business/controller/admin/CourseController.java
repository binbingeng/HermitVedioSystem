package com.hermit.business.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.Course;
import com.hermit.server.domain.CourseContent;
import com.hermit.server.dto.*;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.mapper.CourseContentMapper;
import com.hermit.server.service.CourseCategoryService;
import com.hermit.server.service.CourseService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/course")
public class CourseController {

    private static final Logger LOG = LoggerFactory.getLogger(CourseController.class);
    public static final String BUSINESS_NAME = "课程";
    @Resource
    private CourseService courseService;

    @Resource
    private CourseCategoryService courseCategoryService;

    @PostMapping("/list")
    public ResponseDto list(@RequestBody CoursePageDto pageDto) {
        LOG.info("pageDto: {}", pageDto);
        ResponseDto responseDto = new ResponseDto();
        courseService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody CourseDto courseDto){
        LOG.info("courseDto: {}",courseDto);
        /**保存校验*/
        ValidatorUtil.required(courseDto.getName(),"名称");
        ValidatorUtil.length(courseDto.getName(),"名称",1,50);
        ValidatorUtil.length(courseDto.getSummary(),"概述",1,2000);
        ValidatorUtil.required(courseDto.getPrice(),"价格（元）");
        ValidatorUtil.length(courseDto.getImage(),"封面",1,100);
        ValidatorUtil.required(courseDto.getLevel(),"级别");

        ResponseDto responseDto = new ResponseDto();
        courseService.save(courseDto);
        responseDto.setContent(courseDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
    public ResponseDto delete(@PathVariable String id) {
        LOG.info("id: {}", id);
        ResponseDto responseDto = new ResponseDto();
        courseService.delete(id);
        return responseDto;
    }

    /**
     * 查找课程下所有分类
     **/
    @PostMapping("/list-category/{courseId}")
    public ResponseDto listCategory(@PathVariable(value = "courseId") String courseId) {
        ResponseDto responseDto = new ResponseDto();
        List<CourseCategoryDto> dtoList = courseCategoryService.listByCourse(courseId);
        responseDto.setContent(dtoList);
        return responseDto;
    }

    /**
     * 查询课程内容
     */
    @GetMapping("/find-content/{courseId}")
    public ResponseDto findContent(@PathVariable String courseId) {
        ResponseDto responseDto = new ResponseDto();
        CourseContentDto contentDto = courseService.findContent(courseId);
        responseDto.setContent(contentDto);
        return responseDto;
    }

    /**
     * 修改/新增课程类容
     */
    @PostMapping("/save-content")
    public ResponseDto saveContent(@RequestBody CourseContentDto contentDto) {
        ResponseDto responseDto = new ResponseDto();
        courseService.saveContent(contentDto);
        return responseDto;
    }

    /**
     * 更新排序
     */
    @PostMapping("/sort")
    public ResponseDto sort(@RequestBody SortDto sortDto) {
        LOG.info("更新排序");
        ResponseDto responseDto = new ResponseDto();
        courseService.sort(sortDto);
        return responseDto;
    }
}