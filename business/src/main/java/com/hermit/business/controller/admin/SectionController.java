package com.hermit.business.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.Section;
import com.hermit.server.dto.SectionDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.dto.SectionPageDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.SectionService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/section")
public class SectionController {

    private static final Logger LOG = LoggerFactory.getLogger(SectionController.class);
    public static final String BUSINESS_NAME = "小节";
    @Resource
    private SectionService sectionService;

    @PostMapping("/list")
   public ResponseDto list(@RequestBody SectionPageDto sectionPageDto){
        LOG.info("pageDto: {}",sectionPageDto);
        ResponseDto responseDto = new ResponseDto();
        ValidatorUtil.required(sectionPageDto.getCourseId(),"课程ID");
        ValidatorUtil.required(sectionPageDto.getChapterId(),"大章ID");
        sectionService.list(sectionPageDto);
        responseDto.setContent(sectionPageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody SectionDto sectionDto){
        LOG.info("sectionDto: {}",sectionDto);
        /**保存校验*/
        ValidatorUtil.required(sectionDto.getTitle(),"标题");
        ValidatorUtil.length(sectionDto.getTitle(),"标题",1,50);
        ValidatorUtil.length(sectionDto.getVideo(),"视频",1,200);

        ResponseDto responseDto = new ResponseDto();
        sectionService.save(sectionDto);
        responseDto.setContent(sectionDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        sectionService.delete(id);
        return responseDto;
    }

}