package com.hermit.business.controller;

import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.util.ValidatorUtil;
import com.mysql.jdbc.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 集中异常处理，防止前端页面因为得不到响应数据卡死
 * 异常处理关键注解：ControllerAdvice
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(value = ValidatorException.class)
    @ResponseBody
    public ResponseDto validatorExceptionHandler(ValidatorException e){
        ResponseDto responseDto = new ResponseDto();
        responseDto.setSuccess(false);
        /**
         * 为了安全，避免暴露校验规则，不反回具体校验信息，所以不返回e.getMessage()
         * 处理办法：前端统一回复请求异常
         * 后端打印具体异常
         */
        //responseDto.setMessage(e.getMessage());
        LOG.warn(e.getMessage());
        responseDto.setMessage("请求参数异常！");
        return responseDto;
    }
}
