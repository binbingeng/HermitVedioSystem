Loading = {
    show:function() {
        $.blockUI({
            message: '<h2><img src="/static/image/loading4.gif"/>努力冲呀、加载呀....</h2>',
            css:{
                zIndex:"10011",
                padding:"10%",
                width:'100%',
                height:'100%',
                top:"0%",
                left:"0%",
                opacity: .7,
                backgroundColor: '#000',
                color: '#00BFFF',
            }

        });
    },
    hide:function (){
        //故意延迟
        // setTimeout(function (){
        //     $.unblockUI();
        // }, 500)
       $.unblockUI();
    }
}

Loading1 = {
    show:function() {
        $.blockUI({
            message: '<h2><img src="/static/image/loading1.gif"/>数据正在骑马赶来....</h2>',
            css:{
                zIndex:"10011",
                padding:"10%",
                width:'100%',
                height:'100%',
                top:"0%",
                left:"0%",
                opacity: .6,
                backgroundColor: '#7FFF00',
                color: '#000',
            }

        });
    },
    hide:function (){
        //故意延迟
        // setTimeout(function (){
        //     $.unblockUI();
        // }, 500)
        $.unblockUI();
    }
}

Loading2 = {
    show:function() {
        $.blockUI({
            message: '<h2><img src="/static/image/loading2.gif"/>数据正在骑马赶来....</h2>',
            css:{
                zIndex:"10011",
                padding:"10%",
                width:'100%',
                height:'100%',
                top:"0%",
                left:"0%",
                opacity: .6,
                backgroundColor: '#F08080',
                color: '#B22222',
            }

        });
    },
    hide:function (){
        //故意延迟
        // setTimeout(function (){
        //     $.unblockUI();
        // }, 500)
        $.unblockUI();
    }
}


Loading3 = {
    show:function() {
        $.blockUI({
            message: '<h2><img src="/static/image/loading3.gif"/>数据正在骑马赶来....</h2>',
            css:{
                zIndex:"10011",
                padding:"10%",
                width:'100%',
                height:'100%',
                top:"0%",
                left:"0%",
                opacity: .6,
                backgroundColor: '#F08080',
                color: '#B22222',
            }

        });
    },
    hide:function (){
        //故意延迟
        // setTimeout(function (){
        //     $.unblockUI();
        // }, 500)
        $.unblockUI();
    }
}
