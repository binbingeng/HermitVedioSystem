Validator = {
    //非空校验
    require: function (value,text){
        if(Tool.isEmpty(value)){
            toast.warning(text + "不能为空!");
            return false;
        }else{
            return true;
        }
    },

    //长度校验
    length: function (value,text,min,max){
        if(Tool.isEmpty()){
            return true;
        }
        if(!Tool.isLength(value,min,max)){
            toast.warning(text+"长度"+min+"-"+max+"位!");
            return false;
        }else{
            return true;
        }
    }
};