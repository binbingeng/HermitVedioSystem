/*
* 数组过滤器
* 实例：{{CHARGE | optionKV(section.charge)}}  CHARGE对应list section.charge对应key
* @param Object  {CHARGE:{key:"C",value:"收费"},FREE:{key:"F",value:"免费"}}
* @param key C||F
* @param {string} 收费|免费
* @param yangfanNB
*/

let optionKV = (object,key) =>{
    if(!object || !key){
        return "";
    }else{
        let result="";
        for(let enums in object){
            // console.log(object[enums]["key"]);
            if(key===object[enums]["key"]){
                result = object[enums]["value"];
            }
        }
        return result;
    }
};

/*
* 数组过滤器
* 实例：{{CHARGE | optionKV(section.charge)}}  CHARGE对应list section.charge对应key
* @param List [{key:"C",value:"收费"},{key:"F",value:"免费"}]
* @param key C||F
* @param {string} 收费|免费
* @param yangfanNB
*/
// let optionKV = (list,key) =>{
//     if(!list || !key){
//         return "";
//     }else{
//         let result="";
//         for(let i=0;i<list.length;i++){
//             if(key===list[i]["key"]){
//                 result = list[i]["value"];
//             }
//         }
//         return result;
//     }
// };

/**
 *时长过滤器
 * 数据库中的时长是秒，过滤成时分秒
 * @param value 36000
 * @return {string} 10时0分0秒
 */
let formatSecond = (value) =>{
    value = value || 0;
    let second = parseInt(value,10); //秒
    let minute = 0;
    let hour = 0;

    if(second >= 60){
        minute = Math.floor(second / 60);
        second = Math.floor(second % 60) ;
        if(minute >= 60){
            hour = Math.floor(minute / 60);
            minute = Math.floor(minute % 60);
        }
    }
    let result = ""+second+"秒";
    if(minute>0){
        result = "" + minute + "分" + result;
        if(hour>0){
            result = "" + hour + "时" + result;
        }
    }
    return result;
}
/**
 * 格式化文件大小
 */

let formatFileSize = (value) => {
    value = value || 0;
    let result;
    if(value > 100*1024){
        result = Math.round((value / 1024 / 1024) * 100 ) / 100 + "MB" ;
    } else {
        result = Math.round((value/1024) * 100) / 100 + "KB" ;
    }
    return result;
}

export default {
    optionKV,
    formatSecond,
    formatFileSize,
}
