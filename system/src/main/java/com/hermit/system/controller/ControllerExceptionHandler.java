package com.hermit.system.controller;

import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.BusinessException;
import com.hermit.server.exception.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 集中异常处理，防止前端页面因为得不到响应数据卡死
 * 异常处理关键注解：ControllerAdvice
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResponseDto businessExceptionHandler(BusinessException e){
        ResponseDto responseDto = new ResponseDto();
        responseDto.setSuccess(false);
        /**
         * 为了安全，避免暴露校验规则，不反回具体校验信息，所以不返回e.getMessage()
         * 处理办法：前端统一回复请求异常
         * 后端打印具体异常
         */
        //responseDto.setMessage(e.getMessage());
        LOG.warn("业务异常：{}",e.getCode().getDesc());
        responseDto.setMessage(e.getCode().getDesc());
        return responseDto;
    }
}
