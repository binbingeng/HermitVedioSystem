package com.hermit.system.controller.admin;

import com.github.pagehelper.Page;
import com.hermit.server.domain.RoleResource;
import com.hermit.server.dto.RoleResourceDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.ResponseDto;
import com.hermit.server.exception.ValidatorException;
import com.hermit.server.service.RoleResourceService;
import com.hermit.server.util.ValidatorUtil;
import org.apache.ibatis.annotations.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin/roleResource")
public class RoleResourceController {

    private static final Logger LOG = LoggerFactory.getLogger(RoleResourceController.class);
    public static final String BUSINESS_NAME = "角色资源关联";
    @Resource
    private RoleResourceService roleResourceService;

    @PostMapping("/list")
   public ResponseDto list(@RequestBody PageDto pageDto){
        LOG.info("pageDto: {}",pageDto);
        ResponseDto responseDto = new ResponseDto();
        roleResourceService.list(pageDto);
        responseDto.setContent(pageDto);
        return responseDto;
    }

    @PostMapping("/save")
   public ResponseDto save(@RequestBody RoleResourceDto roleResourceDto){
        LOG.info("roleResourceDto: {}",roleResourceDto);
        /**保存校验*/
        ValidatorUtil.required(roleResourceDto.getRoleId(),"角色");
        ValidatorUtil.required(roleResourceDto.getResource(),"资源");

        ResponseDto responseDto = new ResponseDto();
        roleResourceService.save(roleResourceDto);
        responseDto.setContent(roleResourceDto);
        return responseDto;
    }

    @DeleteMapping("/delete/{id}")
   public ResponseDto delete(@PathVariable String id){
        LOG.info("id: {}",id);
        ResponseDto responseDto = new ResponseDto();
        roleResourceService.delete(id);
        return responseDto;
    }

}