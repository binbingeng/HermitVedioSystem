package com.hermit.server.dto;

/** Dto类 */
public class RoleResourceDto {
    private String id;
    private String roleId;
    private String resource;
    /**set与get方法  */
    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }
    public String getRoleId(){
        return roleId;
    }

    public void setRoleId(String roleId){
        this.roleId = roleId;
    }
    public String getResource(){
        return resource;
    }

    public void setResource(String resource){
        this.resource = resource;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(",id=").append(id);
        sb.append(",roleId=").append(roleId);
        sb.append(",resource=").append(resource);
        sb.append("]");
        return sb.toString();
    }
}