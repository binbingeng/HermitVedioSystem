package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.Category;
import com.hermit.server.domain.CategoryExample;
import com.hermit.server.dto.CategoryDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.CategoryMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Service
public class CategoryService {
    @Resource
    private CategoryMapper categoryMapper;

    /** 查询 带分页*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        CategoryExample categoryExample = new CategoryExample();
        /** 根据sort来排序*/
        categoryExample.setOrderByClause("sort asc");

        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        //查询数据
        PageInfo<Category> pageInfo = new PageInfo<>(categoryList);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<CategoryDto> categoryDtoList = new ArrayList<CategoryDto>();
        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            CategoryDto categoryDto = new CategoryDto();
            BeanUtils.copyProperties(category,categoryDto);
            categoryDtoList.add(categoryDto);
        }
        //设置最终查询结果
        pageDto.setList(categoryDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }

    /** 查询 所有*/
    public List<CategoryDto> all(){
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.setOrderByClause("sort asc");
        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        List<CategoryDto> categoryDtoList = CopyUtil.copyList(categoryList,CategoryDto.class);
        return categoryDtoList;

    }
    /** 保存*/
    public void save(CategoryDto categoryDto){
        Category category = CopyUtil.copy(categoryDto,Category.class);
        if(StringUtils.isEmpty(categoryDto.getId())){
            this.insert(category);
        }else{
            this.update(category);
        }
    
    }
    /**新增 */
    private void insert(Category category){
        Date now = new Date();
        category.setId(UUidUtil.getShortUuid());
        categoryMapper.insert(category);
    }
    /**修改更新 */
    private void update(Category category){
        categoryMapper.updateByPrimaryKey(category);
    }
    /**删除 */
    @Transactional
    public void delete(String id){
        deleteChildren(id);
        categoryMapper.deleteByPrimaryKey(id);
    }
    /**删除子分类*/
    private void deleteChildren(String id){
        Category category = categoryMapper.selectByPrimaryKey(id);
        if("00000000".equals(category.getParent())){
            CategoryExample categoryExample = new CategoryExample();
            categoryExample.createCriteria().andParentEqualTo(category.getId());
            categoryMapper.deleteByExample(categoryExample);
        }
    }

}