package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.Chapter;
import com.hermit.server.domain.ChapterExample;
import com.hermit.server.domain.Course;
import com.hermit.server.dto.ChapterDto;
import com.hermit.server.dto.ChapterPageDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.ChapterMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ChapterService {
    @Resource
    private ChapterMapper chapterMapper;

    public void list(ChapterPageDto chapterPageDto){
        PageHelper.startPage(chapterPageDto.getPage(),chapterPageDto.getSize());
        ChapterExample chapterExample = new ChapterExample();

        ChapterExample.Criteria criteria = chapterExample.createCriteria();
        if(!StringUtils.isEmpty(chapterPageDto.getCourseId())){
            criteria.andCourseIdEqualTo(chapterPageDto.getCourseId());
        }

        List<Chapter> chapterList = chapterMapper.selectByExample(chapterExample);
        //查询数据
        PageInfo<Chapter> pageInfo = new PageInfo<>(chapterList);
        //设置已查询条数
        chapterPageDto.setTotal(pageInfo.getTotal());

        List<ChapterDto> chapterDtoList = new ArrayList<ChapterDto>();
        for (int i = 0; i < chapterList.size(); i++) {
            Chapter chapter = chapterList.get(i);
            ChapterDto chapterDto = new ChapterDto();
            BeanUtils.copyProperties(chapter,chapterDto);
            chapterDtoList.add(chapterDto);
        }
        //设置最终查询结果
        chapterPageDto.setList(chapterDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /*
    新增：没有判断id有没有值，此功能判断id是否有值
     */
    public void save(ChapterDto chapterDto){
        Chapter chapter = CopyUtil.copy(chapterDto,Chapter.class);
//        Chapter chapter= new Chapter();
//        BeanUtils.copyProperties(chapterDto,chapter);
        if(StringUtils.isEmpty(chapterDto.getId())){
            this.insert(chapter);
        }else{
            this.update(chapter);
        }

    }
    /*
    向数据库插入数据
    暴露Dto 不要暴露本身实体类方法 所以不用public 用private  内部使用，
     */
    private void insert(Chapter chapter){
        chapter.setId(UUidUtil.getShortUuid());
//        Chapter chapter= new Chapter();
//        BeanUtils.copyProperties(chapterDto,chapter);
        chapterMapper.insert(chapter);
    }

    private void update(Chapter chapter){
//        Chapter chapter= new Chapter();
//        BeanUtils.copyProperties(chapterDto,chapter);
//        Chapter chapter = CopyUtil.copy(chapterDto,Chapter.class);
        chapterMapper.updateByPrimaryKey(chapter);
    }

    public void delete(String id){
        chapterMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查询一个课程下所有章节
     */
    public List<ChapterDto> listByCourse(String courseId){
        ChapterExample chapterExample = new ChapterExample();
        chapterExample.createCriteria().andCourseIdEqualTo(courseId);
        List<Chapter> chapterList = chapterMapper.selectByExample(chapterExample);
        List<ChapterDto> chapterDtoList = CopyUtil.copyList(chapterList,ChapterDto.class);
        return chapterDtoList;
    }
}
