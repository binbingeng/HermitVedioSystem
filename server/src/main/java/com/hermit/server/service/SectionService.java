package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.Section;
import com.hermit.server.domain.SectionExample;
import com.hermit.server.dto.SectionDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.dto.SectionPageDto;
import com.hermit.server.mapper.SectionMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Date;


@Service
public class SectionService {
    @Resource
    private SectionMapper sectionMapper;

    @Resource
    private CourseService courseService;

    /** 查询*/
    public void list(SectionPageDto sectionPageDto){
        PageHelper.startPage(sectionPageDto.getPage(),sectionPageDto.getSize());
        SectionExample sectionExample = new SectionExample();
        SectionExample.Criteria criteria = sectionExample.createCriteria();
        if(!StringUtils.isEmpty(sectionPageDto.getCourseId())){
            criteria.andCourseIdEqualTo(sectionPageDto.getCourseId());
        }
        if(!StringUtils.isEmpty(sectionPageDto.getChapterId())){
            criteria.andChapterIdEqualTo(sectionPageDto.getChapterId());
        }

        /** 根据sort来排序*/
        sectionExample.setOrderByClause("sort asc");

        List<Section> sectionList = sectionMapper.selectByExample(sectionExample);
        //查询数据
        PageInfo<Section> pageInfo = new PageInfo<>(sectionList);
        //设置已查询条数
        sectionPageDto.setTotal(pageInfo.getTotal());

        List<SectionDto> sectionDtoList = new ArrayList<SectionDto>();
        for (int i = 0; i < sectionList.size(); i++) {
            Section section = sectionList.get(i);
            SectionDto sectionDto = new SectionDto();
            BeanUtils.copyProperties(section,sectionDto);
            sectionDtoList.add(sectionDto);
        }
        //设置最终查询结果
        sectionPageDto.setList(sectionDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    @Transactional
    public void save(SectionDto sectionDto){
        Section section = CopyUtil.copy(sectionDto,Section.class);
        if(StringUtils.isEmpty(sectionDto.getId())){
            this.insert(section);
        }else{
            this.update(section);
        }
        courseService.updateTime(sectionDto.getCourseId());
    }
    /**新增 */
    private void insert(Section section){
        Date now = new Date();
        section.setCreatedAt(now);
        section.setUpdatedAt(now);
        section.setId(UUidUtil.getShortUuid());
        sectionMapper.insert(section);
    }
    /**修改更新 */
    private void update(Section section){
        section.setUpdatedAt(new Date());
        sectionMapper.updateByPrimaryKey(section);
    }
    /**删除 */
    public void delete(String id){
        sectionMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查询一个课程下所有的小节
     */
    public List<SectionDto> listByCourse(String courseId){
      SectionExample sectionExample = new SectionExample();
      sectionExample.createCriteria().andCourseIdEqualTo(courseId);
      List<Section> sectionList = sectionMapper.selectByExample(sectionExample);
      List<SectionDto> sectionDtoList = CopyUtil.copyList(sectionList,SectionDto.class);
      return sectionDtoList;
    }
}