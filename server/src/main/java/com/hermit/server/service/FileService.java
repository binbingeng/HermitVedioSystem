package com.hermit.server.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageAutoDialect;
import com.hermit.server.domain.File;
import com.hermit.server.domain.FileExample;
import com.hermit.server.dto.FileDto;
import com.hermit.server.dto.PageDto;
import com.hermit.server.mapper.FileMapper;
import com.hermit.server.util.CopyUtil;
import com.hermit.server.util.UUidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Date;


@Service
public class FileService {
    @Resource
    private FileMapper fileMapper;

    /** 查询*/
    public void list(PageDto pageDto){
        PageHelper.startPage(pageDto.getPage(),pageDto.getSize());
        FileExample fileExample = new FileExample();
        /** 根据sort来排序*/

        List<File> fileList = fileMapper.selectByExample(fileExample);
        //查询数据
        PageInfo<File> pageInfo = new PageInfo<>(fileList);
        //设置已查询条数
        pageDto.setTotal(pageInfo.getTotal());

        List<FileDto> fileDtoList = new ArrayList<FileDto>();
        for (int i = 0; i < fileList.size(); i++) {
            File file = fileList.get(i);
            FileDto fileDto = new FileDto();
            BeanUtils.copyProperties(file,fileDto);
            fileDtoList.add(fileDto);
        }
        //设置最终查询结果
        pageDto.setList(fileDtoList);
        //return pageDto;  pageDto是从前端传递来的，按道理应该有返回值，此处传入对象==返回对象，属于原地返回（杨凡专用名词）
    }
    /** 保存*/
    public void save(FileDto fileDto){
        File file = CopyUtil.copy(fileDto,File.class);
        File fileKey = selectByKey(fileDto.getKey());
        if(fileKey == null){
            this.insert(file);
        }else{
            fileKey.setShardIndex(fileDto.getShardIndex());
            this.update(fileKey);
        }

    }
    /**新增 */
    private void insert(File file){
        Date now = new Date();
        file.setCreatedAt(now);
        file.setUpdatedAt(now);
        file.setId(UUidUtil.getShortUuid());
        fileMapper.insert(file);
    }
    /**修改更新 */
    private void update(File file){
        file.setUpdatedAt(new Date());
        fileMapper.updateByPrimaryKey(file);
    }
    /**删除 */
    public void delete(String id){
        fileMapper.deleteByPrimaryKey(id);
    }

    /**根据key查找文件*/
    public File selectByKey(String key){
        FileExample example = new FileExample();
        example.createCriteria().andKeyEqualTo(key);
        List<File> fileList = fileMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(fileList)){
            return null;
        } else{
            return fileList.get(0);
        }
    }
    /**根据文件标识查找数据可记录*/
    public FileDto findByKey(String key){
        return CopyUtil.copy(selectByKey(key),FileDto.class);
    }
}