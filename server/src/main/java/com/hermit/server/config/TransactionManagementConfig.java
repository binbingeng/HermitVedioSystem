package com.hermit.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 事务处理类
 * 防止接口对多表操作之间出现异常，导致数据不一致
 */

@EnableTransactionManagement
@Configuration
public class TransactionManagementConfig {

}
