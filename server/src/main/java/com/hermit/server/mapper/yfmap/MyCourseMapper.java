package com.hermit.server.mapper.yfmap;

import com.hermit.server.dto.CourseDto;
import com.hermit.server.dto.CoursePageDto;
import com.hermit.server.dto.SortDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MyCourseMapper {

    List<CourseDto> list(@Param("pageDto") CoursePageDto pageDto);

    int updateTime(@Param("courseId") String courseId);

    int updateSort(SortDto sortDto);

    int moveSortsBackword(SortDto sortDto);

    int moveSortsForward(SortDto sortDto);
}
